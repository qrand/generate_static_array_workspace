use build_script::create_sequence;
use build_script::LowDiscrepancySequence;

fn main() {
    let sequence = create_sequence();
    println!("Sequence elements:");
    for i in 0..10 {
        println!("{}", sequence.element(i, 0).unwrap_or(1.1));
    }
}
