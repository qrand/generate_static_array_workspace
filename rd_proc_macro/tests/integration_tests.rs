#[cfg(test)]
mod integration_tests {
    use rd_proc_macro::create_rd_alphas;

    #[test]
    fn test_proc_macro() {
        create_rd_alphas!(2);

        assert_eq!(0.7548776662466927, ALPHAS[0]);
        assert_eq!(0.5698402909980532, ALPHAS[1]);
    }
}
