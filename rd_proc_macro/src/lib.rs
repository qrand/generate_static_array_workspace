// I need the TokenStream, since it is required by
// the definitions of the procedural macro.
use proc_macro::TokenStream;
// I created another crate which creates the alphas for me.
// I could re-use this crate for other ise cases.
use rd_alphas::create_alphas;

// Convert the dimension TokenStream to a string and then parse this into an usize
fn parse_dimension(dimension: TokenStream) -> usize {
    // parse the token stream as usize literal or panic
    let dim_string = dimension.to_string();
    dim_string.parse::<usize>().unwrap()
}

// create the static alphas array as Rust source code string
fn create_rd_alphas_array(dim: usize) -> String {
    // create a string from the alpha values
    let mut array_string = String::from("static ALPHAS:[f64; ");
    array_string.push_str(dim.to_string().as_str());
    array_string.push_str("] = [\r\n");
    let alphas = create_alphas(dim);
    for alpha in &alphas {
        array_string.push_str("\u{20}\u{20}\u{20}\u{20}");
        array_string.push_str(alpha.to_string().as_str());
        array_string.push_str(",\r\n");
    }
    array_string.push_str("];\r\n");
    array_string
}

#[proc_macro]
pub fn create_rd_alphas(dimension: TokenStream) -> TokenStream {
    let dim = parse_dimension(dimension);

    let array_string = create_rd_alphas_array(dim);

    // create the TokenStream output
    array_string.parse().unwrap()
}

#[proc_macro]
pub fn create_sequence(dimension: TokenStream) -> TokenStream {
    let dim = parse_dimension(dimension);

    let mut code = String::from("{\r\n");

    code.push_str(create_rd_alphas_array(dim).as_str());

    code.push_str("\r\n");
    code.push_str("rd_proc_macro_user_lib::create_seq(&ALPHAS)");

    code.push_str("}\r\n");

    // create the TokenStream output
    code.parse().unwrap()
}
