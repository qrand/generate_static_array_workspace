// the crate which defines the interface for LowDiscrepancySequences
pub use low_discrepancy_sequence::LowDiscrepancySequence;

// my internal, non-public module for the Rd sequence
mod rd;
use rd::new_sequence;

// including the generated alphas.rs
include!(concat!(env!("OUT_DIR"), "/alphas.rs"));

// my public interface which hides all the details about the alphas
pub fn create_sequence() -> impl LowDiscrepancySequence {
    new_sequence(&ALPHAS)
}
