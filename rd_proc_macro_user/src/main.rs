use rd_proc_macro_user_lib::{create_sequence, LowDiscrepancySequence};

fn main() {
    let sequence = create_sequence!(1);
    println!("Sequence elements:");
    for i in 0..10 {
        println!("{}", sequence.element(i, 0).unwrap_or(1.1));
    }
}
