#[macro_export]
macro_rules! create_seq {
    ($dimension:expr) => {{
        use constant_function::{create_sequence, create_sequence_data};
        const ALPHAS: [f64; $dimension] = create_sequence_data::<$dimension>();
        let sequence = create_sequence(&ALPHAS);
        sequence
    }};
    () => {
        create_seq! {1}
    };
}
