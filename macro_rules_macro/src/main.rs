use low_discrepancy_sequence::LowDiscrepancySequence;
use macro_rules_macro::create_seq;

fn main() {
    let seq = create_seq!();
    println!("First element {}", seq.element(0, 0).unwrap_or(1.1));
}
