#[cfg(test)]
mod tests {
    use low_discrepancy_sequence::LowDiscrepancySequence;
    use macro_rules_macro::create_seq;
    #[test]
    fn test_macro() {
        const dim: usize = 2;
        let sequence = create_seq!(dim);
        assert_eq!(0.0, sequence.element(0, 0).unwrap_or(1.1));
        assert_eq!(0.0, sequence.element(0, 1).unwrap_or(1.1));
    }

    #[test]
    fn test_macro_without_arguments() {
        let sequence = create_seq!();
        assert_eq!(0.0, sequence.element(0, 0).unwrap_or(1.1));
        assert!(sequence.element(0, 1).is_err());
    }
}
