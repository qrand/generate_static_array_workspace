#![no_builtins]

extern crate core;
use core::result::{Result, Result::Err, Result::Ok};

extern crate low_discrepancy_sequence;
use low_discrepancy_sequence::{LowDiscrepancySequence, QrandCoreError};

struct Rd<'a> {
    dimension: usize,
    alphas: &'a [f64],
}

pub fn new_sequence<'a>(alphas: &'a [f64]) -> impl LowDiscrepancySequence + 'a {
    Rd::new(alphas)
}

impl<'a> Rd<'a> {
    fn new(alphas: &'a [f64]) -> impl LowDiscrepancySequence + 'a {
        Rd {
            dimension: alphas.len(),
            alphas: alphas,
        }
    }
}

impl<'a> LowDiscrepancySequence for Rd<'a> {
    fn element(&self, n: usize, dim: usize) -> Result<f64, QrandCoreError> {
        if dim < self.dimension {
            let value = n as f64 * self.alphas[dim];
            if value < 1.0 {
                Ok(value)
            } else {
                let integer_part = (value as u64) as f64;
                Ok(value - integer_part)
            }
        } else {
            Err(QrandCoreError::create_point_element_not_existing())
        }
    }
}
