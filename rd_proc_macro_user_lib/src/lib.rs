// the crate which defines the interface for LowDiscrepancySequences
pub use low_discrepancy_sequence::LowDiscrepancySequence;

// Procedural macro for creating alphas
pub use rd_proc_macro::create_rd_alphas;
//pub use rd_proc_macro::create_sequence;

// my internal, non-public module for the Rd sequence
mod rd;
use rd::new_sequence;

#[macro_export]
macro_rules! create_sequence {
    ($dimension:expr) => {{
        $crate::create_rd_alphas!($dimension);
        $crate::create_seq(&ALPHAS)
    }};
    () => {
        create_sequence! {1}
    };
}

pub fn create_seq(alphas: &'static [f64]) -> impl LowDiscrepancySequence {
    new_sequence(&alphas)
}
