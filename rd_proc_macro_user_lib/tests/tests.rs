#[cfg(test)]
mod tests {
    use rd_proc_macro_user_lib::{create_sequence, LowDiscrepancySequence};
    #[test]
    fn test_macro() {
        let sequence = create_sequence!(2);
        assert_eq!(0.0, sequence.element(0, 0).unwrap_or(1.1));
        assert_eq!(0.0, sequence.element(0, 1).unwrap_or(1.1));
        assert_eq!(0.7548776662466927, sequence.element(1, 0).unwrap_or(1.1));
        assert_eq!(0.5698402909980532, sequence.element(1, 1).unwrap_or(1.1));
        assert_eq!(0.5097553324933854, sequence.element(2, 0).unwrap_or(1.1));
        assert_eq!(0.13968058199610645, sequence.element(2, 1).unwrap_or(1.1));
    }

    //#[test]
    //fn test_macro_without_arguments() {
    //    let sequence = create_sequence!();
    //    assert_eq!(0.0, sequence.element(0, 0).unwrap_or(1.1));
    //    assert!(sequence.element(0, 1).is_err());
    //}
}
