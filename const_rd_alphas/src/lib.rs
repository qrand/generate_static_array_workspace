#![feature(const_fn)]
#![feature(const_generics)]
#![feature(const_mut_refs)]

const fn powf(base: f64, exp: f64) -> f64 {
    1.0 //base.powf(exp)
}

const fn powu(base: f64, exp: usize) -> f64 {
    if exp == 0 {
        1.0
    } else {
        let mut i = 1;
        let mut x = base;
        while i < exp {
            x *= base;
            i += 1;
        }
        x
    }
}

const fn fract(value: f64) -> f64 {
    if value < 1.0 {
        value
    } else {
        let integer_part = (value as u64) as f64;
        value - integer_part
    }
}

const fn recip(value: f64) -> f64 {
    1.0 / value
}

//const fn calculate_phi_for(dimension: usize) -> f64 {
//    let mut phi: f64 = 2.0;
//    // power is always 1 / (dimension + 1)
//    let power: f64 = f64::from(dimension as u32 + 1).recip();
//    // phi is approximated, I loop 25 times to get a reasonable approximation on my machine
//    let mut i = 0;
//    while i < 25 {
//        phi += 1.0;
//        phi = phi.powf(power);
//        i += 1;
//    }
//    phi
//}

const fn calculate_phi(dimension: usize) -> f64 {
    let mut phi: f64 = 2.0;
    // power is always 1 / (dimension + 1)
    let power: f64 = recip((dimension + 1) as f64);
    let mut i = 0;
    // phi is approximated, I loop 25 times to get a reasonable approximation on my machine
    while i < 25 {
        phi = phi + 1.0;
        phi = powf(phi, power);
        i += 1;
    }
    phi
}

const fn create_array<const DIM: usize>() -> [f64; DIM] {
    [0.0; DIM]
}

pub const fn create_alphas<const DIM: usize>() -> [f64; DIM] {
    let dimension = DIM as u32;
    let mut array: [f64; DIM] = [0.0; DIM];
    let phi = calculate_phi(DIM);
    let inv_g = recip(phi);
    let mut i = 0;
    while i < dimension {
        array[i as usize] = fract(powu(inv_g, i as usize + 1));
        i += 1;
    }
    array
}

pub const fn fill_alphas(alphas: &mut [f64]) {
    let dimension = alphas.len();
    let phi = calculate_phi(dimension);
    let inv_g = recip(phi);
    let mut i = 0;
    while i < dimension {
        alphas[i as usize] = fract(powu(inv_g, i as usize + 1));
        i += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::{create_alphas, powu};
    #[test]
    fn test_powu() {
        assert_eq!(4.0, powu(2.0, 2));
        assert_eq!(16.0, powu(4.0, 2));
        assert_eq!(16.0, powu(2.0, 4));
        assert_eq!(95367431640625.0, powu(5.0, 20));
    }

    #[test]
    fn test_create_alphas() {
        let alphas = create_alphas::<2>();
        assert_eq!(0.7548776662466927, alphas[0]);
        assert_eq!(0.5698402909980532, alphas[1]);
    }
}
