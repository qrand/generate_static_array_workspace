#![feature(const_fn)]
#![feature(const_generics)]

use const_rd_alphas::{create_alphas, fill_alphas};
use low_discrepancy_sequence::LowDiscrepancySequence;

mod rd;
use rd::new_sequence;

pub const fn create_sequence(sequence_data: &'static [f64]) -> impl LowDiscrepancySequence {
    new_sequence(&sequence_data)
}

//pub const fn create_seq<const DIM: usize>() -> impl LowDiscrepancySequence {
//    let mut alphas: [f64; DIM] = [0.0; DIM];
//    fill_alphas(&mut alphas);
//    static ALPHAS: [f64; DIM] = alphas;
//    new_sequence(&ALPHAS)
//}

pub const fn create_sequence_data<const DIM: usize>() -> [f64; DIM] {
    [0.0; DIM]
}
