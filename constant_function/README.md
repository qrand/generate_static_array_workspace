# Constant function

* Install nightly Rust

```shell
rustup install nightly
```

* Build with nightly

```shell
cargo +nightly <CARGO COMMAND>
```
