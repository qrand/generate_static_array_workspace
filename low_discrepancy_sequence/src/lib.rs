#![crate_type = "staticlib"]
#![no_std]
#![no_builtins]
#![no_implicit_prelude]
#![forbid(unsafe_code)]

mod error;
pub use error::QrandCoreError;
mod low_discrepancy_sequence;
pub use low_discrepancy_sequence::LowDiscrepancySequence;
